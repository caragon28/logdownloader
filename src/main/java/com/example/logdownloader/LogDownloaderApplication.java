package com.example.logdownloader;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ListObjectsV2Request;
import com.amazonaws.services.s3.model.ListObjectsV2Result;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.io.File;


@SpringBootApplication
public class LogDownloaderApplication {

    private static final Logger LOG = LoggerFactory.getLogger(LogDownloaderApplication.class);

    /**
     * Execute program
     *
     * @param args args[0]: year, p.e. 2021
     *             args[1]: month, p.e. 01, 02, 03, ..., 12
     *             args[2]: day, p.e. 01, 02, 03, .... 31
     */
    public static void main(String[] args) {
        if (args.length != 3) {
            LOG.error("You must be set 3 arguments:\n\targs[0] -> year (2021)" +
                    "\n\targs[1] -> month (06)\n\targs[2] -> day (07)");
            System.exit(0);
        }
        SpringApplication.run(LogDownloaderApplication.class, args);
    }

    @Bean
    public CommandLineRunner demo(final AmazonS3 s3Client,
                                  final @Value("${aws.awsBucketName}") String bucket,
                                  final @Value("${aws.file.name.start}") String fileName,
                                  final @Value("${aws.start.prefix}") String prefix,
                                  final @Value("${output.path}") String path,
                                  final @Value("${aws.pattern.name}") String pattern) {
        return (args) -> {
            String year = args[0];
            String month = args[1];
            String day = args[2];

            LOG.info("INITIALIZE DOWNLOAD");

            String fileNameDownload = fileName + args[0] + args[1] + args[2] + "T";
            String startPrefix = prefix + year + "/" + month + "/" + day + "/";

            File folder = new File(path + "/" + year + month + day);

            if (folder.exists()) {
                LOG.error("Folder {}{}{} exists on: {}", year, month, day, path);
                System.exit(0);
            } else {
                folder.mkdir();
                for (int hour = 0; hour <= 23; hour++) {
                    String hourNormalized = hour < 10 ? "0" + hour : String.valueOf(hour);
                    ListObjectsV2Request req = new ListObjectsV2Request().withBucketName(bucket)
                            .withPrefix(startPrefix + fileNameDownload + hourNormalized)
                            .withDelimiter("/");
                    LOG.info(startPrefix + fileNameDownload + hourNormalized);
                    ListObjectsV2Result listing = s3Client.listObjectsV2(req);
                    int i = 1;
                    for (S3ObjectSummary summary : listing.getObjectSummaries()) {
                        String keyFile = summary.getKey();
                        String finalFileName = keyFile.substring(keyFile.lastIndexOf("/"));
                        finalFileName = finalFileName.substring(finalFileName.indexOf(pattern)).replace(pattern + "_", "");
                        File file = new File(folder + "/" + finalFileName);
                        GetObjectRequest getObjectRequest = new GetObjectRequest(bucket, keyFile);
                        s3Client.getObject(getObjectRequest, file);
                        System.out.println("Downloading [" + (i++) + ":" + listing.getObjectSummaries().size() + "] file");
                    }
                }
            }
            LOG.info("FINISH DOWNLOAD");
        };
    }

}
